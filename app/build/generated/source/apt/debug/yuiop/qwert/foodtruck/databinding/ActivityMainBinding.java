package yuiop.qwert.foodtruck.databinding;
import yuiop.qwert.foodtruck.R;
import yuiop.qwert.foodtruck.BR;
import android.view.View;
public class ActivityMainBinding extends android.databinding.ViewDataBinding implements android.databinding.generated.callback.OnClickListener.Listener {

    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView, 5);
    }
    // views
    public final android.widget.ImageButton bpic;
    public final android.widget.EditText etNumber;
    public final android.widget.LinearLayout llMain;
    private final android.widget.Button mboundView4;
    public final android.widget.TextView textView;
    public final android.widget.TextView tvName;
    // variables
    private yuiop.qwert.foodtruck.viewModel.MainActivityViewmodel mViewModel;
    private final android.view.View.OnClickListener mCallback2;
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private android.databinding.InverseBindingListener etNumberandroidTextAttrChanged = new android.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.phoneNumber.get()
            //         is viewModel.phoneNumber.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = android.databinding.adapters.TextViewBindingAdapter.getTextString(etNumber);
            // localize variables for thread safety
            // viewModel.phoneNumber
            android.databinding.ObservableField<java.lang.String> viewModelPhoneNumber = null;
            // viewModel.phoneNumber != null
            boolean viewModelPhoneNumberJavaLangObjectNull = false;
            // viewModel.phoneNumber.get()
            java.lang.String viewModelPhoneNumberGet = null;
            // viewModel
            yuiop.qwert.foodtruck.viewModel.MainActivityViewmodel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPhoneNumber = viewModel.phoneNumber;

                viewModelPhoneNumberJavaLangObjectNull = (viewModelPhoneNumber) != (null);
                if (viewModelPhoneNumberJavaLangObjectNull) {




                    viewModelPhoneNumber.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ActivityMainBinding(android.databinding.DataBindingComponent bindingComponent, View root) {
        super(bindingComponent, root, 2);
        final Object[] bindings = mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds);
        this.bpic = (android.widget.ImageButton) bindings[3];
        this.bpic.setTag(null);
        this.etNumber = (android.widget.EditText) bindings[2];
        this.etNumber.setTag(null);
        this.llMain = (android.widget.LinearLayout) bindings[0];
        this.llMain.setTag(null);
        this.mboundView4 = (android.widget.Button) bindings[4];
        this.mboundView4.setTag(null);
        this.textView = (android.widget.TextView) bindings[5];
        this.tvName = (android.widget.TextView) bindings[1];
        this.tvName.setTag(null);
        setRootTag(root);
        // listeners
        mCallback2 = new android.databinding.generated.callback.OnClickListener(this, 2);
        mCallback1 = new android.databinding.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean setVariable(int variableId, Object variable) {
        switch(variableId) {
            case BR.viewModel :
                setViewModel((yuiop.qwert.foodtruck.viewModel.MainActivityViewmodel) variable);
                return true;
        }
        return false;
    }

    public void setViewModel(yuiop.qwert.foodtruck.viewModel.MainActivityViewmodel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }
    public yuiop.qwert.foodtruck.viewModel.MainActivityViewmodel getViewModel() {
        return mViewModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelUserName((android.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelPhoneNumber((android.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelUserName(android.databinding.ObservableField<java.lang.String> ViewModelUserName, int fieldId) {
        switch (fieldId) {
            case BR._all: {
                synchronized(this) {
                        mDirtyFlags |= 0x1L;
                }
                return true;
            }
        }
        return false;
    }
    private boolean onChangeViewModelPhoneNumber(android.databinding.ObservableField<java.lang.String> ViewModelPhoneNumber, int fieldId) {
        switch (fieldId) {
            case BR._all: {
                synchronized(this) {
                        mDirtyFlags |= 0x2L;
                }
                return true;
            }
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.databinding.ObservableField<java.lang.String> viewModelUserName = null;
        java.lang.String viewModelPhoneNumberGet = null;
        java.lang.String viewModelUserNameGet = null;
        android.databinding.ObservableField<java.lang.String> viewModelPhoneNumber = null;
        yuiop.qwert.foodtruck.viewModel.MainActivityViewmodel viewModel = mViewModel;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.userName
                        viewModelUserName = viewModel.userName;
                    }
                    updateRegistration(0, viewModelUserName);


                    if (viewModelUserName != null) {
                        // read viewModel.userName.get()
                        viewModelUserNameGet = viewModelUserName.get();
                    }
            }
            if ((dirtyFlags & 0xeL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.phoneNumber
                        viewModelPhoneNumber = viewModel.phoneNumber;
                    }
                    updateRegistration(1, viewModelPhoneNumber);


                    if (viewModelPhoneNumber != null) {
                        // read viewModel.phoneNumber.get()
                        viewModelPhoneNumberGet = viewModelPhoneNumber.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.bpic.setOnClickListener(mCallback1);
            android.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.etNumber, (android.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (android.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (android.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, etNumberandroidTextAttrChanged);
            this.mboundView4.setOnClickListener(mCallback2);
        }
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.etNumber, viewModelPhoneNumberGet);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.tvName, viewModelUserNameGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // viewModel
                yuiop.qwert.foodtruck.viewModel.MainActivityViewmodel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.saveUserDetails();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                yuiop.qwert.foodtruck.viewModel.MainActivityViewmodel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.profilePick();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    public static ActivityMainBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ActivityMainBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ActivityMainBinding>inflate(inflater, yuiop.qwert.foodtruck.R.layout.activity_main, root, attachToRoot, bindingComponent);
    }
    public static ActivityMainBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ActivityMainBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(yuiop.qwert.foodtruck.R.layout.activity_main, null, false), bindingComponent);
    }
    public static ActivityMainBinding bind(android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ActivityMainBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/activity_main_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ActivityMainBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): viewModel.userName
        flag 1 (0x2L): viewModel.phoneNumber
        flag 2 (0x3L): viewModel
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}