package yuiop.qwert.foodtruck.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;

import java.util.Arrays;

import yuiop.qwert.foodtruck.R;
import yuiop.qwert.foodtruck.databinding.ActivityMainBinding;
import yuiop.qwert.foodtruck.viewModel.MainActivityViewmodel;

public class MainActivity extends AppCompatActivity {

    MainActivityViewmodel viewmodel = new MainActivityViewmodel();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(MainActivity.this,R.layout.activity_main);
        binding.setViewModel(viewmodel);
        viewmodel.OnCreate(MainActivity.this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        viewmodel.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewmodel.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewmodel.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        viewmodel.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewmodel.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        viewmodel.onActivityResult(requestCode,resultCode,data);

    }


    public void hello()
    {

    }
}
