package yuiop.qwert.foodtruck.viewModel;


import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Arrays;

import yuiop.qwert.foodtruck.model.UserDetails;
import yuiop.qwert.foodtruck.view.MainActivity;

import static android.app.Activity.RESULT_OK;

/**
 * Created by kaushal on 16/9/17.
 */

public class MainActivityViewmodel implements ViewModel {

    private UserDetails userDetails;

    //Firbase Objects
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private FirebaseStorage firebaseStorage;
    private StorageReference mStorageReference;
    private UploadTask uploadTask;

    //Loggin State
    private static boolean LOGGED_IN ;
    private final String ANONYMOUS = "Anonymous";
    private static String name;


    //Image url
    private Uri selectedImageFile;


    //Activity requestCode
    private final int FIREBASE_SIGN_IN = 1;
    private final int IMAGE_PICKER = 2;

    MainActivity context;

    public final ObservableField<String> userName = new ObservableField<>();
    public final ObservableField<String> phoneNumber = new ObservableField<>("");

    @Override
    public void OnCreate(MainActivity context) {

        this.context = new MainActivity();
        this.context = context;

        //Firebase instances
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseStorage =FirebaseStorage.getInstance();

        //Database references on Firebase database
        databaseReference = firebaseDatabase.getReference().child("userDetails");
        mStorageReference = firebaseStorage.getReference().child("profilePics");


        name = ANONYMOUS;

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user!=null)
                {
                    //User logged in
                    signinInitialization(user.getDisplayName());
                }
                else{
                    //User logged out
                    signoutInitilialization();
                }

                userName.set("Hello, "+name);
            }
        };

    }

    private void signinInitialization(String username) {
        LOGGED_IN = true;
        name = username;

    }

    private void signoutInitilialization() {
        LOGGED_IN = false;
        name = ANONYMOUS;

        //User Logged Out
        Toast.makeText(context,"You are Logged Out", Toast.LENGTH_SHORT).show();
        context.startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().setIsSmartLockEnabled(false)
                .setAvailableProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                        new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                .build(), FIREBASE_SIGN_IN);

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

        firebaseAuth.addAuthStateListener(authStateListener);

    }

    @Override
    public void onPause() {

        if(authStateListener!=null)
        {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }


    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==FIREBASE_SIGN_IN && resultCode == RESULT_OK)
        {
            //User Logged In
            Toast.makeText(context, "You are Logged In!", Toast.LENGTH_SHORT).show();
        }
        else if(requestCode == IMAGE_PICKER && resultCode ==RESULT_OK)
        {
            selectedImageFile = data.getData();

        }
    }


    public void saveUserDetails()
    {
        //Not profile pic is selected
        if(selectedImageFile !=null) {

            //Phone number is not 10 digit number
            if(phoneNumber.get().length() !=10) {

                StorageReference storageReference = mStorageReference.child(selectedImageFile.getLastPathSegment());
                uploadTask = storageReference.putFile(selectedImageFile);

                //ACtion complete Listener
                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Uri filename = taskSnapshot.getDownloadUrl();

                        //Userdetails { String Username, String phonenumber, String profilePicUrl, float latitude, float longitude}
                        userDetails = new UserDetails(name, phoneNumber.get(), filename.toString(), 0, 0);
                        databaseReference.push().setValue(userDetails);
                        Toast.makeText(context, "User details saved.", Toast.LENGTH_SHORT).show();

                    }
                });

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, "User details could not be uploaded.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else
                Toast.makeText(context, "Phone number is not valid", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(context, "No profile pic selected.", Toast.LENGTH_SHORT).show();

    }


    public void profilePick()
    {
        Intent imagePicker = new Intent(Intent.ACTION_GET_CONTENT);
        imagePicker.setType("image/jpeg");
        imagePicker.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
        context.startActivityForResult(Intent.createChooser(imagePicker,"Choose your Profile pic"),IMAGE_PICKER);

    }
}
