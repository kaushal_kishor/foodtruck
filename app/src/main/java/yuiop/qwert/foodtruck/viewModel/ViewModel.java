package yuiop.qwert.foodtruck.viewModel;

import android.content.Context;
import android.content.Intent;

import yuiop.qwert.foodtruck.view.MainActivity;

/**
 * Created by kaushal on 16/9/17.
 */

public interface ViewModel {
    void OnCreate(MainActivity context);

    void onStart();

    void onResume();

    void onPause();

    void onStop();

    void onDestroy();

    void onActivityResult(int requestCode, int resultCode, Intent data);

}
